
// code block: taquin15.cpp


// code block taquin-include

#include <iostream>
#include <vector>
#include <set>
#include <queue>
#include <map>
#include <utility>
#include <list>
#include <functional>
#include <cmath>
#include <algorithm>
#include <utility>
#include <iomanip>
#include <limits>

using namespace std;


// code block taquin-types

typedef vector<int> State;

//  .---.
//  |2|0|
//  .---. -> State b = {2,0,1,3}
//  |1|3|
//  .---.

typedef function<int( const State& pos )> Heuristic;


// code block taquin-side

int 
side( const State& b )
{
  double y = sqrt( b.size() );
  int x = y;
  return x;
}


// code block taquin-heuristics

int 
breadth( const State& b )
{
  return 0;
}

int
nbmis( const State& b )
{
  int d = 0;
  for( int i = 0 ; i < b.size() ; i++ )
  {
    if( b[i] != 0 ) // not a tile, '0' doesn't count
    {
      if( b[i] != i ) d++;
    }
  }
  return d;
}

int 
manh( const State& b )
{
  int d = 0;
  int s = side(b);
  for( int i = 0 ; i < b.size() ; i++ )
  {
    if( b[i] != 0 )  // not a tile, '0' doesn't count
    {
      d += abs( i / s - b[i] / s ) +
           abs( i % s - b[i] % s );
    }
  }
  return d;
}


// code block taquin-final_state

bool
final_state( const State& b )
{
  return (nbmis(b) == 0); // we use nbmis for it is quick to compute
}


// code block taquin-print

void
print( const State& state )
{
  int s = side(state);
  for( int i = 0 ; i < state.size() ; i++ )
  {
    if( i % s == 0 ) cout << endl;
    cout << setw(2) << setfill('0') << state[i] << " , ";
  }
  cout << endl;
}


// code block taquin-astar-dist

int
dist( const map<State,int>& d, const State& state )
{
  map< State , int >::const_iterator it = d.find(state);
  if( d.end() == it )
  {
    return numeric_limits<int>::max();
  }
  return it->second;
}


// code block taquin-astar

void
astar( const State& initial_state, 
       Heuristic    h,
       list<State>  *best_path,
       int          *nbiter )
{
  
  // code block taquin-astar-init
  
  int s = side(initial_state);
  
  map< State , State > parent;
  parent[initial_state] = initial_state;
  
  map< State , int > d;
  d[initial_state] = h(initial_state);
  
  set< State > black;
  
  priority_queue< pair< int , State >,
                  vector< pair< int, State > >,
                  greater< pair< int, State > > > grey;
  grey.push( { d[initial_state], initial_state } ); // initially the source is grey
  
  vector<State> neighbors;
  
  (*nbiter) = 0;
  



  while( !grey.empty() )
  {
    State current_state = grey.top().second;

    (*nbiter)++;

    if( final_state(current_state) )
    {
      
      // code block taquin-astar-stop
      
      best_path->clear();
      while( current_state != initial_state )
      {
        best_path->push_front(current_state);
        current_state = parent[current_state];
      }
      best_path->push_front(initial_state);
      
      return;
    }

    black.insert(current_state); // current_state becomes black...
    grey.pop();     // ...it is no more grey

    
    // code block taquin-astar-neighnors
    
    neighbors.clear();
    
    int pos0 = find( current_state.begin(), current_state.end(), 0 ) - current_state.begin();
    
    if( (pos0 + 1) < current_state.size() &&
        ((pos0 + 1) % s) != 0 )
    {
      State neighbor = current_state;
      swap( neighbor[pos0] , neighbor[pos0 + 1] );
      neighbors.push_back(neighbor);
    }
    
    if( (pos0 - 1) >= 0 &&
        ((pos0 - 1) % s) != (s-1) )
    {
      State neighbor = current_state;
      swap( neighbor[pos0] , neighbor[pos0 - 1] );
      neighbors.push_back(neighbor);
    }
    
    if( (pos0 + s) < current_state.size() )
    {
      State neighbor = current_state;
      swap( neighbor[pos0] , neighbor[pos0 + s] );
      neighbors.push_back(neighbor);
    }
    
    if( (pos0 - s) >= 0 )
    {
      State neighbor = current_state;
      swap( neighbor[pos0] , neighbor[pos0 - s] );
      neighbors.push_back(neighbor);
    }
    

    for( State neighbor : neighbors )
    {
      
      // code block taquin-astar-update
      
      if( black.end() != black.find(neighbor) ) // when neighbor n of u is black...
      {
        continue;                // ...nothing to do
      }
                                 // when it is white or grey...
      int new_cost = d[current_state] + 1 - h(current_state) + h(neighbor);
      if( dist(d,neighbor) > new_cost )   // ...we may update the shortest known distance to it
      {
        d[neighbor] = new_cost;
        parent[neighbor] = current_state;
        grey.push( { d[neighbor] , neighbor } );
      }
      
    }

  }
}


int
main()
{
  //State b = {4,8,3,2,0,7,6,5,1}; // C0 small problem easily solved by A*
  //State b = {7,11,8,3,14,0,6,15,1,4,13,9,5,12,2,10}; // C1 easy configuration that can also be solved by A*
  State b = {14,10,9,4,13,6,5,8,2,12,7,0,1,3,11,15}; // C2 more difficult configuration that will not be solved by A* (with 6 GB of RAM)

  list<State> best_path;
  int nbiter = 0;
  
  astar(b, manh, &best_path, &nbiter);
  cout << "Heuristic manh:" << endl;
  cout << "nb moves: " << best_path.size()-1 << endl;
  cout << "nb nodes explored: " << nbiter << endl;

  return 0;
}
