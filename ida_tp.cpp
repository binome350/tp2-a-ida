// code block taquin-include

#include <iostream>
#include <vector>
#include <set>
#include <queue>
#include <map>
#include <utility>
#include <list>
#include <functional>
#include <cmath>
#include <algorithm>
#include <utility>
#include <iomanip>
#include <limits>

using namespace std;


// code block taquin-types

typedef vector<int> State;

//  .---.
//  |2|0|
//  .---. -> State b = {2,0,1,3}
//  |1|3|
//  .---.

typedef function<int( const State& pos) > Heuristic;


// code block taquin-side

int
side(const State& b)
{
    double y = sqrt(b.size());
    int x = y;
    return x;
}


// code block taquin-heuristics

int
breadth(const State& b)
{
    return 0;
}

int
nbmis(const State& b)
{
    int d = 0;
    for (int i = 0; i < b.size(); i++)
    {
        if (b[i] != 0) // not a tile, '0' doesn't count
        {
            if (b[i] != i) d++;
        }
    }
    return d;
}

int
manh(const State& b)
{
    int d = 0;
    int s = side(b);
    for (int i = 0; i < b.size(); i++)
    {
        if (b[i] != 0) // not a tile, '0' doesn't count
        {
            d += abs(i / s - b[i] / s) +
                    abs(i % s - b[i] % s);
        }
    }
    return d;
}


// code block taquin-final_state

bool
final_state(const State& b)
{
    return (nbmis(b) == 0); // we use nbmis for it is quick to compute
}


// code block taquin-print

void
print(const State& state)
{
    int s = side(state);
    for (int i = 0; i < state.size(); i++)
    {
        if (i % s == 0) cout << endl;
        cout << setw(2) << setfill('0') << state[i] << " , ";
    }
    cout << endl;
}


// code block taquin-astar-dist

int
dist(const map<State, int>& d, const State& state)
{
    map< State, int >::const_iterator it = d.find(state);
    if (d.end() == it)
    {
        return numeric_limits<int>::max();
    }
    return it->second;
}

// code block taquin-ida-search

void
search(const State& current_state,
       int ub, // upper bound over which exploration must stop
       int& nub,
       list<State>& path,
       list<State>& best_path,
       Heuristic h,
       int& nb_visited_state)
{
    //cout << "> Longueur " << path.size() << endl;
    nb_visited_state++;

    if (final_state(current_state))
    {
        best_path = path;
        return;
    }

    //Get the neighbors ! 
    // code block taquin-astar-neighnors
    vector<State> neighbors;
    int s = side(current_state);
    neighbors.clear();

    int pos0 = find(current_state.begin(), current_state.end(), 0) - current_state.begin();

    if ((pos0 + 1) < current_state.size() &&
            ((pos0 + 1) % s) != 0)
    {
        State neighbor = current_state;
        swap(neighbor[pos0], neighbor[pos0 + 1]);
        neighbors.push_back(neighbor);
    }

    if ((pos0 - 1) >= 0 &&
            ((pos0 - 1) % s) != (s - 1))
    {
        State neighbor = current_state;
        swap(neighbor[pos0], neighbor[pos0 - 1]);
        neighbors.push_back(neighbor);
    }

    if ((pos0 + s) < current_state.size())
    {
        State neighbor = current_state;
        swap(neighbor[pos0], neighbor[pos0 + s]);
        neighbors.push_back(neighbor);
    }

    if ((pos0 - s) >= 0)
    {
        State neighbor = current_state;
        swap(neighbor[pos0], neighbor[pos0 - s]);
        neighbors.push_back(neighbor);
    }


    for (State neighbor : neighbors)
    {
        //Renvoit un itérateur sur là où l'objet est, ou alors path.end() si pas d'objets
        if (find(path.begin(), path.end(), neighbor) != path.end())
        {
            //cout << " NEIGHBOR " << endl;
            //print(neighbor);
            //cout << " Path " << endl;
            //print(*find(path.begin(), path.end(), neighbor));
            //If the neighbor is in the path
        }
        else
        {
            //Path.size = profondeur (g) + cout de l'actuel au suivant (=1) Car dans path, il y a le noeud "0"
            int f = path.size() + h(neighbor);
            //cout << " F : " << f << endl;
            if (f > ub)
            {
                //cout << " Upper band " << ub << " et f " << f << endl;

                if (f < nub)
                {
                    nub = f;
                    //cout << "Next Upper band " << nub << endl;

                }
            }
            else
            {
                path.push_back(neighbor);
                search(neighbor, ub, nub, path, best_path, h, nb_visited_state);
                path.pop_back();
                if (!best_path.empty())
                {
                    //cout << "Solution trouvé" << endl;
                    return;
                }
            }
        }


    }
    return;
}



// code block taquin-ida

void
ida(const State& initial_state,
    Heuristic h,
    list<State>& best_path, // path from source to destination
    int& nb_visited_state)
{

    int ub; // current upper bound
    //We don't want to go in the wrong direction
    int nub = h(initial_state); // next upper bound
    //Best path is empty right now. 

    //Start point in the actual path
    list<State> path;
    path.push_back(initial_state); // so that the reconstructed path starts with the source

    while (best_path.empty() && nub != numeric_limits<int>::max())
    {
        ub = nub;
        nub = numeric_limits<int>::max();

        cout << "upper bound: " << ub;
        search(initial_state, ub, nub, path, best_path, h, nb_visited_state);
        cout << " ; nb_visited_state: " << nb_visited_state << endl;
    }

}

int
main()
{
    //State b = {4, 8, 3, 2, 0, 7, 6, 5, 1}; // C0 small problem easily solved by A*
    //State b = {7,11,8,3,14,0,6,15,1,4,13,9,5,12,2,10}; // C1 easy configuration that can also be solved by A*
    State b = {14,10,9,4,13,6,5,8,2,12,7,0,1,3,11,15}; // C2 more difficult configuration that will not be solved by A* (with 6 GB of RAM)

    list<State> best_path;
    int nb_visited_state = 0;

    ida(b, manh, best_path, nb_visited_state);
    cout << "nb moves: " << best_path.size() - 1 << endl;
    cout << "nb visited states: " << nb_visited_state << endl;

    for (list<State>::iterator it = best_path.begin();
            it != best_path.end(); it++)
    {
        print((*it));
    }

    best_path.clear();

    return 0;
}
