class Configuration
{
public:
    Configuration();
    // Constructeur par defaut creant une configuration avec 0 bloc et 0 pile

    Configuration(int b, int p);
    // Constructeur creant une configuration avec b blocs et p piles

    Configuration(const Configuration& c);
    // Constructeur creant une copie de c

    void setInitial();
    // initialise this avec la configuration initiale (blocs uniformement repartis sur les piles)

    void setFinal();
    // initialise this avec la configuration finale (tous les blocs sur nbPiles-1)

    void transition(int p1, int p2);
    // precondition : !pileVide(p1)
    // Applique l'action (p1->p2) a this

    void affiche() const;
    // Affiche this

    bool operator<(const Configuration&) const;

    bool operator>(const Configuration&) const;

    bool operator==(const Configuration&) const;

    bool operator!=(const Configuration&) const;

    int heuristique(const Configuration& cf) const;
    // retourne une borne inferieure de la distance minimale de this a cf

    int getNbPiles();
    // retourne le nombre de piles

    bool pileVide(int p);
    // retourne true ssi p est vide

    ~Configuration();
    // destructeur

private:
    int nbBlocs, nbPiles;
    int* pile; // pour chaque bloc b : pile[b] = pile dans laquelle se trouve b
    int* suivant; // pour chaque bloc b : si b est le bloc le plus bas de sa pile,
    // alors suivant[b]=-1, sinon suivant[b]=numero du bloc immediatement sous b
    int* sommet; // pour chaque pile p : si p est vide alors sommet[p]=-1
    // sinon sommet[i]=numero du bloc au sommet de i
    int compare(const int*, const int*, const int*) const;
};

Configuration::Configuration()
{
    // Constructeur par defaut
    nbBlocs = 0;
    nbPiles = 0;
}

Configuration::Configuration(int b, int p)
{
    // Constructeur creant une configuration avec b blocs et p piles
    nbBlocs = b;
    nbPiles = p;
    pile = new int[nbBlocs];
    suivant = new int[nbBlocs];
    sommet = new int[nbPiles];
}

Configuration::Configuration(const Configuration& c)
{
    // Constructeur creant une copie de c
    nbBlocs = c.nbBlocs;
    nbPiles = c.nbPiles;
    pile = new int[nbBlocs];
    suivant = new int[nbBlocs];
    sommet = new int[nbPiles];
    for (int p = 0; p < nbPiles; p++)
    {
        sommet[p] = c.sommet[p];
    }
    for (int b = 0; b < nbBlocs; b++)
    {
        suivant[b] = c.suivant[b];
        pile[b] = c.pile[b];
    }
}

Configuration::~Configuration()
{
    delete[] suivant;
    delete[] pile;
    delete[] sommet;
}

void Configuration::setInitial()
{
    // initialise this avec la configuration initiale (blocs uniformement repartis sur les piles)
    for (int p = 0; p < nbPiles; p++)
    {
        sommet[p] = -1;
    }
    for (int b = 0; b < nbBlocs; b++)
    {
        int p = b % nbPiles; // on met le bloc b sur la pile p
        pile[b] = p;
        suivant[b] = sommet[p];
        sommet[p] = b;
    }
}

void Configuration::setFinal()
{
    // initialise this avec la configuration finale (tous les blocs sur nbPiles-1)
    for (int p = 0; p < nbPiles - 1; p++)
    {
        sommet[p] = -1;
    }
    sommet[nbPiles - 1] = 0;
    for (int b = 0; b < nbBlocs; b++)
    {
        pile[b] = nbPiles - 1;
        suivant[b] = b + 1;
    }
    suivant[nbBlocs - 1] = -1;
}

void Configuration::transition(int p1, int p2)
{
    // precondition : sommet[p1] != -1
    // Applique l'action (p1->p2) a this
    int b1 = sommet[p1];
    int sb1 = suivant[b1];
    int b2 = sommet[p2];
    suivant[b1] = b2;
    sommet[p2] = b1;
    sommet[p1] = sb1;
    pile[b1] = p2;
}

void affichePile(int p, int* suivant)
{
    // affiche les blocs de la pile p, du plus bas au plus haut
    if (p >= 0)
    {
        affichePile(suivant[p], suivant);
        cout << p << " ";
    }
}

void Configuration::affiche() const
{
    for (int p = 0; p < nbPiles; p++)
    {
        cout << "pile " << p << " :";
        affichePile(sommet[p], suivant);
        cout << endl;
    }
}

int Configuration::heuristique(const Configuration& cf) const
{
    int sommeTotale = 0;
    for (int i = 0; i < cf.nbBlocs; i++)
    {
        //On prend le suivant du bloc courant
        int blocActuel = this->suivant[i];
        bool existeUnBlocEnPositionFinale = false;
        bool existeUnBlocMalPlace = false;

        // On parcourt la pile entière à la recherche d'un bloc bien placé.
        while (blocActuel != -1)
        {
            //Si la pile du bloc actuelle est la pile de sa position finale = Si ce bloc est bien placé
            if (this->pile[blocActuel] == cf.pile[blocActuel])
            {
                existeUnBlocEnPositionFinale = true;
            }
            else
            { //Si le bloc est mal placé
                existeUnBlocMalPlace = true;
            }
            blocActuel = this->suivant[blocActuel];
        }

        //Est il bien placé ? 
        if (this->pile[i] == cf.pile[i])
        {
            //On est dans le troisième cas
            sommeTotale += 2;
        }
        else
        {
            //Si il existe un en dessous qui n'appartient pas à la position finale.
            //Pour tous les blocs du jeu
            for (int j = 0; j < cf.nbBlocs; j++)
            {
                //Si le bloc courant est dans la pile du bloc qu'on regarde now
                if (this->pile[j] == this->pile[i])
                {
                    if (existeUnBlocEnPositionFinale)
                    {
                        //On est dans le cas 2
                        sommeTotale += 2;
                    }
                    else
                    {
                        //On est dans le cas 1
                        sommeTotale += 1;
                    }

                }
            }
        }

    }

    return 0;
}

int Configuration::getNbPiles()
{
    return nbPiles;
}

bool Configuration::pileVide(int p)
{
    return sommet[p] == -1;
}

bool Configuration::operator<(const Configuration& c) const
{
    // retourne true si this < c
    return c.compare(pile, sommet, suivant) < 0;
}

bool Configuration::operator>(const Configuration& c) const
{
    // retourne true si this > c
    return c.compare(pile, sommet, suivant) > 0;
}

bool Configuration::operator==(const Configuration& c) const
{
    // retourne true si this == c
    return c.compare(pile, sommet, suivant) == 0;
}

bool Configuration::operator!=(const Configuration& c) const
{
    // retourne true si this != c
    return c.compare(pile, sommet, suivant) != 0;
}

int Configuration::compare(const int* pile2, const int* sommet2, const int* suivant2) const
{
    for (int b = 0; b < nbBlocs; b++)
    {
        if (pile[b] < pile2[b]) return -1;
        if (pile[b] > pile2[b]) return 1;
        if (suivant[b] < suivant2[b]) return -1;
        if (suivant[b] > suivant2[b]) return 1;
    }
    for (int p = 0; p < nbPiles; p++)
    {
        if (sommet[p] < sommet2[p]) return -1;
        if (sommet[p] > sommet2[p]) return 1;
    }
    return 0;
}
