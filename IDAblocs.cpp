#include <iostream>
using namespace std;
#include "config.cpp"
#include <iostream>
#include <vector>
#include <set>
#include <queue>
#include <map>
#include <utility>
#include <list>
#include <functional>
#include <cmath>
#include <algorithm>
#include <utility>
#include <iomanip>
#include <limits>




//================================================================

typedef function<int( const Configuration& pos, const Configuration& final_state_final) > Heuristic;

// code block taquin-heuristics

int
heuristiqueQuatre(const Configuration& b, const Configuration& final_state_final)
{
    return b.heuristique(final_state_final);
}


// code block taquin-final_state

bool
final_state(const Configuration& actual, const Configuration& final_state_final)
{
    return (actual == final_state_final); // we use nbmis for it is quick to compute
}

// code block taquin-astar-dist

int
dist(const map<Configuration, int>& d, const Configuration& state)
{
    map< Configuration, int >::const_iterator it = d.find(state);
    if (d.end() == it)
    {
        return numeric_limits<int>::max();
    }
    return it->second;
}

// code block taquin-ida-search

void
search(Configuration& current_state,
       int ub, // upper bound over which exploration must stop
       int& nub,
       list<Configuration>& path,
       list<Configuration>& best_path,
       Heuristic h,
       int& nb_visited_state, Configuration& final_state_final)
{
    //cout << "> Longueur " << path.size() << endl;
    nb_visited_state++;

    if (final_state(current_state, final_state_final))
    {
        best_path = path;
        return;
    }

    // code block taquin-astar-neighnors
    vector<Configuration> neighbors;
    neighbors.clear();

    //On génère toutes les transitions possibles
    for (int p = 0; p < current_state.getNbPiles(); p++)
    {
        for (int t = 0; t < current_state.getNbPiles(); t++)
        {
            if (!current_state.pileVide(p) && p != t)
            {
                Configuration neighbor = current_state;
                neighbor.transition(p, t);
                neighbors.push_back(neighbor);
            }
        }
    }

    for (Configuration neighbor : neighbors)
    {
        //Renvoit un itérateur sur là où l'objet est, ou alors path.end() si pas d'objets
        if (find(path.begin(), path.end(), neighbor) != path.end())
        {
            //cout << " NEIGHBOR " << endl;
            //print(neighbor);
            //cout << " Path " << endl;
            //print(*find(path.begin(), path.end(), neighbor));
            //If the neighbor is in the path
        }
        else
        {
            //Path.size = profondeur (g) + cout de l'actuel au suivant (=1) Car dans path, il y a le noeud "0"
            int f = path.size() + heuristiqueQuatre(neighbor,final_state_final);
            //cout << " F : " << f << endl;
            if (f > ub)
            {
                //cout << " Upper band " << ub << " et f " << f << endl;

                if (f < nub)
                {
                    nub = f;
                    //cout << "Next Upper band " << nub << endl;

                }
            }
            else
            {
                path.push_back(neighbor);
                search(neighbor, ub, nub, path, best_path, h, nb_visited_state, final_state_final);
                path.pop_back();
                if (!best_path.empty())
                {
                    //cout << "Solution trouvé" << endl;
                    return;
                }
            }
        }


    }
    return;
}

void
ida(Configuration& initial_state,
    Heuristic h,
    list<Configuration>& best_path, // path from source to destination
    int& nb_visited_state, Configuration& final_state_final)
{

    int ub; // current upper bound
    //We don't want to go in the wrong direction
    int nub = heuristiqueQuatre(initial_state,final_state_final); // next upper bound
    //Best path is empty right now. 

    //Start point in the actual path
    list<Configuration> path;
    path.push_back(initial_state); // so that the reconstructed path starts with the source

    while (best_path.empty() && nub != numeric_limits<int>::max())
    {
        ub = nub;
        nub = numeric_limits<int>::max();

        cout << "upper bound: " << ub;
        search(initial_state, ub, nub, path, best_path, h, nb_visited_state, final_state_final);
        cout << " ; nb_visited_state: " << nb_visited_state << endl;
    }

}

int main()
{
    int nbPiles = 3;
    int nbBlocs = 8;
    Configuration c0 = Configuration(nbBlocs, nbPiles);
    c0.setInitial();
    Configuration cf = Configuration(nbBlocs, nbPiles);
    cf.setFinal();
    
    /*int nbPiles = 3;
    int nbBlocs = 4;
    Configuration c0 = Configuration(nbBlocs, nbPiles);
    c0.setInitial();
    cout << "Configuration initiale c0 :" << endl;
    c0.affiche();
    Configuration c1 = Configuration(c0);
    c1.transition(2, 1);
    cout << "Configuration c1 = t(c0,2->1) :" << endl;
    c1.affiche();
    Configuration c2 = Configuration(c1);
    c2.transition(0, 2);
    cout << "Configuration c2 = t(c1,0->2) :" << endl;
    c2.affiche();
    Configuration c3 = Configuration(c2);
    c3.transition(1, 2);
    cout << "Configuration c3 = t(c2,1->2) :" << endl;
    c3.affiche();
    Configuration c4 = Configuration(c3);
    c4.transition(1, 2);
    cout << "Configuration c4 = t(c3,1->2) :" << endl;
    c4.affiche();
    Configuration c5 = Configuration(c4);
    c5.transition(0, 2);
    cout << "Configuration c5 = t(c4,0->2) :" << endl;
    c5.affiche();
    Configuration cf = Configuration(nbBlocs, nbPiles);
    cf.setFinal();
    if (c5 == cf)
    {
        cout << "c5 est egale a la configuration finale..." << endl;
    }*/

    // 

    list<Configuration> best_path;
    int nb_visited_state = 0;

    ida(c0, heuristiqueQuatre, best_path, nb_visited_state, cf);
    cout << "nb moves: " << best_path.size() - 1 << endl;
    cout << "nb visited states: " << nb_visited_state << endl;

    for (list<Configuration>::iterator it = best_path.begin();
            it != best_path.end(); it++)
    {
        (*it).affiche();
    }

    best_path.clear();

    // 

    return 0;
}