#include <iostream>
using namespace std;
#include "config.cpp"

int main()
{
    int nbPiles = 3;
    int nbBlocs = 4;
    Configuration c0 = Configuration(nbBlocs, nbPiles);
    c0.setInitial();
    cout << "Configuration initiale c0 :" << endl;
    c0.affiche();
    Configuration c1 = Configuration(c0);
    c1.transition(2, 1);
    cout << "Configuration c1 = t(c0,2->1) :" << endl;
    c1.affiche();
    Configuration c2 = Configuration(c1);
    c2.transition(0, 2);
    cout << "Configuration c2 = t(c1,0->2) :" << endl;
    c2.affiche();
    Configuration c3 = Configuration(c2);
    c3.transition(1, 2);
    cout << "Configuration c3 = t(c2,1->2) :" << endl;
    c3.affiche();
    Configuration c4 = Configuration(c3);
    c4.transition(1, 2);
    cout << "Configuration c4 = t(c3,1->2) :" << endl;
    c4.affiche();
    Configuration c5 = Configuration(c4);
    c5.transition(0, 2);
    cout << "Configuration c5 = t(c4,0->2) :" << endl;
    c5.affiche();
    Configuration cf = Configuration(nbBlocs, nbPiles);
    cf.setFinal();
    if (c5 == cf)
    {
        cout << "c5 est egale a la configuration finale..." << endl;
    }

    return 0;
}
